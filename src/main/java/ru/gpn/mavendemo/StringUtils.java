package ru.gpn.mavendemo;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StringUtils {
    public List<String> filterShortStrings(List<String> list) {
        return list.stream().filter(s -> s.length() > 3).collect(Collectors.toList());
//        return null;
    }

    public boolean login(String login, String password) {
        return !login.isEmpty() && !password.isEmpty();
    }
}
