package ru.gpn.mavendemo;

//String -> is email or not? boolean
public class EmailValidator {

    public boolean validateEmail(String emailToBeValidated) {

        return emailToBeValidated != null && emailToBeValidated.contains(".");
    }
}
