package ru.gpn.mavendemo.app;

public enum BurgerComponent {
    SALAD,
    BUN,
    CHEESE,
    MEAT,
    KETCHUP
}
