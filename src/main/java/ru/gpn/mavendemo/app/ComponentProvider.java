package ru.gpn.mavendemo.app;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ComponentProvider {

    static final Logger LOGGER = LogManager.getLogger(ComponentProvider.class);

    public static List<BurgerComponent> generateBurgerComponentsPack(int compnentCount) {
        Random rnd = new Random();
        List<BurgerComponent> burgerComponents = new ArrayList<>();

        BurgerComponent[] components = BurgerComponent.values();

        for (int i = 0; i < compnentCount; i++) {
            int randomIndex = rnd.nextInt(components.length);
//            System.out.println("random index = " + randomIndex);
            LOGGER.debug("random index = " + randomIndex);
            LOGGER.debug("generated compnent = " + components[randomIndex]);

            burgerComponents.add(components[randomIndex]);
        }

        LOGGER.info(String.format("Generated burger component pack: %s", burgerComponents));
        return burgerComponents;
    }

    public static List<List<BurgerComponent>> generateBurgerComponentsPacks(int packCount) {
        List<List<BurgerComponent>> packs = new ArrayList<>();

        for(int i = 0; i < packCount; i++){
            packs.add(generateBurgerComponentsPack(5));
        }

//        LOGGER.info(String.format("Generated a number of packs: %s", packs));
        LOGGER.info("Generated a number of packs: {}", packs);

        return packs;
    }
}
