package ru.gpn.mavendemo.app;

import java.util.List;

public class PackValidator {
    public static boolean validate(List<BurgerComponent> burgerComponents) {
        long bunCount = burgerComponents.stream().filter(p -> p.equals(BurgerComponent.BUN)).count();
        return bunCount >= 2 && bunCount <= 3;
    }
}
