package ru.gpn.mavendemo;

import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

//TDD Test driven development

//DDT data driven testing - dataProvider
public class EmailValidatorTests {

    //hooks хуки
    //BeforeMethod
    //BeforeClass
    //BeforeGroups

    @BeforeTest()
    public void setUpTest() {
        System.out.println("executed @BeforeTest");
    }



    @BeforeGroups({"db"})
    public void setUpGroups() {
        System.out.println("prepared DB connection");
    }



    @Test(groups = {"db", "integration", "smoke"})
    public void testValidEmail() {
        String validEmail = "mail@yandex.ru";
        EmailValidator emailValidator = new EmailValidator();
        boolean isEmailValid = emailValidator.validateEmail(validEmail);

        Assert.assertTrue(isEmailValid, "Passed valid email, but validator evaluated it as invalid"); //TODO
    }

    @Test(
            description = "Тест невалидного емейла",
            groups = {"db", "integration"}
//            enabled = false
    )
    public void testInvalidEmail() {
        String validEmail = "mail@yandex";
        EmailValidator emailValidator = new EmailValidator();
        boolean isEmailValid = emailValidator.validateEmail(validEmail);

        Assert.assertFalse(isEmailValid, "Validator did not recognize invalid email");
    }

    @Test(description = "Тест на null значение в качестве email")
    public void testNullEmail() {
        String validEmail = null;
        EmailValidator emailValidator = new EmailValidator();
        boolean isEmailValid = emailValidator.validateEmail(validEmail);

        Assert.assertFalse(isEmailValid, "Validator did not recognize null value");
    }

    @Test(timeOut = 2000)
    public void testApplicationPerformance() {
        System.out.println("user clicked image link");

//        try {
////            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        System.out.println("image page opened");
    }


}
