package ru.gpn.mavendemo;

import org.testng.Assert;
import org.testng.annotations.*;
//suite.xml testng.xml
//TDD Test driven development
public class EmailValidatorTestsWithClassHook {

    EmailValidator emailValidator;

    @BeforeClass
    public void setUp() {
        System.out.println("Executed @BeforeClass");
        emailValidator = new EmailValidator();
    }
    @BeforeMethod
    public void setUpMethod() {
        System.out.println("Executed @BeforeMethod");
        emailValidator = new EmailValidator();
    }

    @AfterClass
    public void tearDown() {
        System.out.println("Executed @AfterClass");
        emailValidator = null;
    }

    @Test
    public void testValidEmail() {
        String validEmail = "mail@yandex.ru";
        boolean isEmailValid = emailValidator.validateEmail(validEmail);
        System.out.println("executing test");
        Assert.assertTrue(isEmailValid, "Passed valid email, but validator evaluated it as invalid"); //TODO
    }

    @Test(description = "Тест невалидного емейла")
    public void testInvalidEmail() {
        String validEmail = "mail@yandex";
        boolean isEmailValid = emailValidator.validateEmail(validEmail);
        System.out.println("executing test");
        Assert.assertFalse(isEmailValid, "Validator did not recognize invalid email");
    }

    @Test(description = "Тест на null значение в качестве email")
    public void testNullEmail() {
        String validEmail = null;
        boolean isEmailValid = emailValidator.validateEmail(validEmail);

        Assert.assertFalse(isEmailValid, "Validator did not recognize null value");
    }

    @Test(timeOut = 2000)
    public void testApplicationPerformance() {
        System.out.println("user clicked image link");

//        try {
////            Thread.sleep(3000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }

        System.out.println("image page opened");
    }


}
