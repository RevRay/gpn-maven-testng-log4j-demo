package ru.gpn.mavendemo;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.util.ArrayList;
import java.util.List;

public class StringUtilsTest {
    @Test
    public void positiveTest() {
        //1. подготовка тестовых данных и логика
        List<String> words = new ArrayList<>();
        words.add("we");
        words.add("are");
        words.add("really");
        words.add("great");
        words.add("programmers");

        List<String> expectedResult = List.of(
                "really",
                "great",
                "progr1ammers"
        );


        StringUtils stringUtils = new StringUtils();
        List<String> result = stringUtils.filterShortStrings(words);

//        //2. assertions
//        Assert.assertNotNull(result);
////        Assert.assertTrue(result != null);
////        Assert.assertTrue(result.equals(expectedResult));
//        Assert.assertEquals(result.size(), expectedResult.size());
//        Assert.assertEquals(result, expectedResult);


        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(result, expectedResult);
        softAssert.assertNotNull(result);
        softAssert.assertEquals(result.size(), expectedResult.size());
        softAssert.assertEquals(result, expectedResult);
        softAssert.assertAll();

    }

    @Test
    public void positiveTest1() {
        //1. подготовка тестовых данных и логика
        List<String> words = new ArrayList<>();
        words.add("we");
        words.add("are");
        words.add("really");
        words.add("great");
        words.add("programmers");

        StringUtils stringUtils = new StringUtils();
        List<String> result = stringUtils.filterShortStrings(words);
    }

//    @Test
//    public void testLogin() {
//        StringUtils stringUtils = new StringUtils();
//        boolean result = stringUtils.login("abc", "123");
//        Assert.assertTrue(result);
//    }
//    @Test
//    public void testLogin1() {
//        StringUtils stringUtils = new StringUtils();
//        boolean result = stringUtils.login("abc1", "1213");
//        Assert.assertTrue(result);
//    }
//    @Test
//    public void testLogin2() {
//        StringUtils stringUtils = new StringUtils();
//        boolean result = stringUtils.login("absdfc", "1f23");
//        Assert.assertTrue(result);
//    }

    //DDT
    @Test(dataProvider = "dataProvider", groups = {"db"})
    public void testLogins(String user, String password) {
        StringUtils stringUtils = new StringUtils();
        boolean result = stringUtils.login(user, password);
        Assert.assertTrue(result);
    }

    @DataProvider
    public Object[][] dataProvider() {
        return new String[][]{
                {"Alex", "fsddf"},
                {"Bob", "3243"},
                {"Bob", "3243"}
        };
    }


}
