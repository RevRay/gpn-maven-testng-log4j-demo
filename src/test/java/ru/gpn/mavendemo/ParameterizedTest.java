package ru.gpn.mavendemo;

import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class ParameterizedTest {

//    public static void main(String[] args) {
//        new ParameterizedTest().testAuthorization();
//    }
    //FRONT  -  DEV   TEST    UAT    PROD
    @Test
    @Parameters({"env", "port"})
    public void testAuthorization(String URL, String port) {
        System.out.println("Going to app url: " + URL + " port number: " + port);
    }
    @Test
    @Parameters({"env", "port", "userDb"})
    public void testAuthorization2(String URL, String port, @Optional("ADMIN") String userDb) {
        System.out.println("Going to app url: " + URL + " port number: " + port);
    }

}
